---
layout: post
title: "TiddlyDrive V0.1.3 Changelog"
categories: [code, tiddlydrive]
issue: 4
---

## New Stuff

* New news tab in the settings dialogue showing the official Twitter feed.

* Improve the general layout of the settings dialogue.

* Add functionality for showing helpful errors e.g. reminding you to unblock popups for the page so that you can authenticate with Google.

* Introduce a versioning system which allows keeping track of updates such as this one. You can see the current version in the settings page, under the title. It's pretty hard to miss.

### Technical note
This is just relevant to developers so feel free to skip.

The stable app is still at https://tiddlydrive.github.io/ but you can access the development server by adding `development` to the end of the URL:

https://tiddlydrive.github.io/development

**Warning: It will probably have untested functionality so use with caution and only if you know what you're doing**

For now, it uses the same cookie storage as the stable app so you probably shouldn't use both at the same time.

## Fixes

* Fix the user-interface of the settings dialogue by polishing some minor issues.

* Neaten code a bit (there's still a lot to be done).