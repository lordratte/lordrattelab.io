---
layout: post
title: "TiddlyDrive Returns!"
categories: [code, tiddlydrive]
issue: 3
---

[Intro](#intro)

[What makes it more stable?](#what-makes-it-more-stable-this-time)

[Have Issues?](#have-issues)

[New feature](#new-feature)

# Intro

If you've tried to use TiddlyDrive since the 15th of January this year, you would have noticed that you get an error message while trying to access it. You may also have seen my termination post on my [official twitter feed](https://twitter.com/Lord_Ratte).

I am glad to announce that TiddlyDrive is back, up-and-running. It'll be more reliable this time.

# What makes it more stable this time?

[Skip to tl;dr](#too-long-didnt-read)

## Technical version

I'm glad you asked. Since last time, I have done away with the dedicated domain name: <s>tiddlydrive.lordratte.info</s>. The new app is located at [https://tiddlydrive.github.io](https://tiddlydrive.github.io) which is the default address for a GitHub pages site.

The observant of you may notice that this was the development site. It no longer complains that it is the dev version because I have changed the code that checks the site to consider this one as production.

For now, if you want to submit a pull request, to the [dev](https://github.com/tiddlydrive/tiddlydrive.github.io/tree/dev) branch.

So what does make it more stable?
Because I am using default and free functionality of GitHub and Google's API, with minimum intervention worrying about certificates, the site isn't as heavily dependent on my regular intervention for it's continuous working. This is good because it functions despite me having a strange/irregular schedule and no funding.

The only case something will go wrong is when GitHub or Google bring out backwards-incompatible changes or they crash...


## Too long; didn't read

The app is hosted on the default domain instead of a fancy one that I have to maintain the whole time.

# Have issues?

If you have a minor problem with a link, typo etc. and are able to, please submit a pull request.

Otherwise please create an issue here: [https://github.com/tiddlydrive/tiddlydrive.github.io/issues](https://github.com/tiddlydrive/tiddlydrive.github.io/issues)

# New feature!

Something new you can do now is open your TiddlyWiki files from the Google Drive app on your phone. On the burger menu beside the file, click "Open With" and select TiddlyDrive. Now you can edit it in your phone's browser easier.