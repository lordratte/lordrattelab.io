---
layout: post
title: "TiddlyDrive V0.1.5 Changelog"
categories: [code, tiddlydrive]
---

## New Stuff

There is a new news tracker that doesn't use a closed-source API.

Check it out under Settings -> News
