---
layout: post
title: "TiddlyDrive V0.1.4 Changelog"
categories: [code, tiddlydrive]
---

## New Stuff
There is a new dontation method, as requested (albeit late).

Since the inception of the project there have been no donations towards the success of the project which, with such a big user-base as TiddlyDrive has, I hope is because there just wasn't an easy enough way to do it. If you like the project and want to support, check it out under the about tab.


## Fixes

* Strange UI bug fixes

* Minor fixes for Firefox (there are still a few to tackle)

* Refactor main javascript file

* Other minor fixes


## Note:
I'm always looking for new contributors. If you are keen, check out the [issues](https://github.com/tiddlydrive/tiddlydrive.github.io/issues) for ones marked "help wanted" or "good first issue".
