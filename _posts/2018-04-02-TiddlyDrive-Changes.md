---
layout: post
title: "TiddlyDrive Changes"
categories: [code, tiddlydrive]
issue: 2
---
So this year, up until now, there were two major issues with TiddlyDrive. Both of these issues have been fixed.

The one issue was an SSL certificate issue with my hosting provider. Those who used TiddlyDrive during this time would have noticed the app's page being blocked with a warning message saying that it wasn't secure. This has been fixed on the host's side and should no longer be a problem.

The other issue was an error showing up whenever a tiddler was edited on Firefox (on certain operating systems). This is fixed with a workaround but, until the root cause of the limitation is fixed by Mozilla, there are two features that won't work on Firefox:

1. The hash of the url in the address bar won't be set to permalinks/permaviews
2. The title of the browser page won't update to the name of the TiddlyWiki until the page is reloaded.

Some people have mentioned difficulties in using TiddlyDrive on iPads. This is something that I'll need to investigate.

# Extra info

The official information page is [here](/tiddlydrive).

If you want to submit a bug report, it can be done [here](https://github.com/tiddlydrive/tiddlydrive.github.io/issues).

I will post any news about updates or downtime on [this blog](/categories/code/) and on my new Twitter feed ([@Lord_Ratte](https://twitter.com/Lord_Ratte)).
