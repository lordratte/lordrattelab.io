---
layout: page
title: Tiddly-Chrome Ex
subtitle: Edit TiddlyWiki from Chrome
permalink: /tiddlychromeex/
---
*[Jump to the installation guide](#installation-guide)*

*[Jump to the tips](#tips)*

*[Jump to the info and links](#info-and-links)*

-----
# Note
This project has been canceled due to lack of interest.

---


Tiddly-Chrome Ex is a Google Chrome extension that lets you import, store and edit TiddlyWiki files. If the file is a [TiddlyWiki file](https://tiddlywiki.com/), then saving automatically writes to the extension's storage.

This app is still being tested so there may be bugs. You can log query or bug report [on the Store page](https://chrome.google.com/webstore/detail/obecgdpdefbmlcciknldmocdmonmbbeb).


# Installation Guide

1. Open [Google Chrome](chrome.google.com).

2. Open the TiddlyWiki file as usual.

3. Click on the Tiddly-Chrome Ex Icon in the top right ![TCEx Icon](/assets/img/tiddlychromeex_guide/icon128.png)

4. Click "Import Current Page" and, from now on, open the wiki from the list in the extension. It will be stored in the extension's storage.

5. Perform a test edit and save to make sure that everything is working properly.


# Tips

## Which browsers are supported?

Obviously, since this is a Chrome extension, you'll be hard-pressed to find a different browser that supports it. Prefer newer versions of Chrome to ensure optimal stability.

# Info and Links

If you want to submit a bug report, it can be done [here](https://chrome.google.com/webstore/detail/obecgdpdefbmlcciknldmocdmonmbbeb).

I will post any news about updates or downtime on [this blog](/categories/code/) but, more reliably, on my new Twitter feed ([@Lord_Ratte](https://twitter.com/Lord_Ratte)).
