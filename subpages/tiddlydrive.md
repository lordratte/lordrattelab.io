---
layout: page
title: TiddlyDrive
subtitle: Edit TiddlyWiki from Google Drive
permalink: /tiddlydrive/
---
*[Jump to the installation guide](#installation-guide)*

*[Jump to the tips](#tips)*

*[Jump to the info and links](#info-and-links)*

-----

[TiddlyDrive](https://tiddlydrive.github.io/) is a web app that has been integrated with Google Drive. Once the app has been connected to One's Google Drive account, one can open html files with this app. If the file is a [TiddlyWiki](https://tiddlywiki.com/) file, then saving automatically writes back to your cloud drive.

This app is still being tested so there may be bugs. You can log query or bug report [on the development repository](https://github.com/tiddlydrive/tiddlydrive.github.io).

![Screenshot of a loaded wiki](/assets/img/tiddlydrive_screenshot1.jpg)

![Screenshot of the options menu](/assets/img/tiddlydrive_screenshot2.jpg)


# Installation Guide

1. Log into [Google Drive](//drive.google.com)

2. Navigate to the folder where your tiddlywiki html file is: ![Step 1](/assets/img/tiddlydrive_guide/step_1.png)

3. Right click on the file and choose "Connect more apps": ![Step 2](/assets/img/tiddlydrive_guide/step_2.png)

4. Search for TiddlyDrive in the app-store window that pops up and connect the app to your account: ![Step 3](/assets/img/tiddlydrive_guide/step_3.png)

5. Right click on your file again and open with the newly added app "TiddlyDrive".

6. You will be taken to the app's website. If you do not get prompted to authorize the app to open the file with your Google account, try allowing popups on the site and refreshing: ![Step 4](/assets/img/tiddlydrive_guide/step_4.png)

7. Perform a test edit and save to make sure that everything is working properly.


# Tips

## Which browsers are supported?

Chrome is the most reliable browser to use with TiddlyDrive. Firefox works but doesn't update the browser-title until the page is reloaded and hash changes don't show up in the url. These limitations will likely be made fix-able in the future. If you have any feedback on your experience of using TiddlyDrive on other browsers, feel free to [DM me on Twitter](https://twitter.com/messages/compose?recipient_id=980719729471082496&text=).

# Info and Links

If you want to submit a bug report, it can be done [here](https://github.com/tiddlydrive/tiddlydrive.github.io/issues).

I will post any news about updates or downtime on [this blog](/categories/code/) and on my Twitter feed ([@Lord_Ratte](https://twitter.com/Lord_Ratte)).
